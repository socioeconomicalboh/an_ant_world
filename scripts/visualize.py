import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation

import numpy as np

def load_anim_data(filename: str):
    # return somehow a 3D array
    # shape is (nFrames, width, height)
    
# somehow read filename and out_filename
    
anim_data = load_anim_data(filename)

assert(len(anim_data.shape) == 3)

# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure( figsize=anim_data.shape[1:] )

starting_frame = anim_data[0]
im = plt.imshow(starting_frame, interpolation='none', aspect='auto', vmin=0, vmax=1)

def animate_func(i: int):
    im.set_array(anim_data[i])
    return [im]

fps = 30
anim = animation.FuncAnimation(
    fig, 
    animate_func, 
    frames = anim_data.shape[0],
    interval = 1000 / fps, # in ms
)

# out_filename should be mp4
anim.save(out_filename, fps=fps, extra_args=['-vcodec', 'libx264'])


