#!/usr/bin/env python
"""Tests for `agent` module."""


import unittest
import numpy as np
#from click.testing import CliRunner

#from an_ant_world import cli
from an_ant_world.environment import CartesianEnvironment
from an_ant_world.agent import RandomWalker, SmellyWalker, AvoidingWalker
from an_ant_world.agent import step_random
from an_ant_world.simulation import GridSimulation

class TestAgent(unittest.TestCase):
    """Tests for `an_ant_world` package."""


    def test_random_walker(self):
        """Test RandomWalker (not avoiding)."""
        n_agents = 2
        n_timesteps = 10
        agents = {RandomWalker() for iid in range(n_agents)}
        positions = [(5, 5) for ag in agents]
        sim = GridSimulation(
            agents_set=agents,
            total_timesteps=n_timesteps,
            environment=CartesianEnvironment(sizes=(10, 10)),
            agents_positions=positions)
        self.assertEqual(sim.run_simulation(), n_timesteps)

    def test_step_avoiding(self):
        """ Test the function step_random. """
        view = {'space': np.array([
            [-1., -1., -1., -1., -1.],
            [0., 0., 0., -1., -1.],
            [0., 0., 1., 0., -1.],
            [0., -1., 0., -1., -1.],
            [0., 0., 0., -1., -1.]
        ])}

        n_tests = 10
        for _ in range(n_tests):
            test_result = (
                np.isin(
                    step_random(view, avoid=True)['where'],
                    [[-1, -1], [-1, 0], [0, -1], [1, 0], [0, 1]]
                    ).all())
            self.assertTrue(test_result.all())

    def test_avoiding_walker(self):
        """Test Avoiding walker."""
        n_agents = 7
        n_timesteps = 10

        agents = {AvoidingWalker() for iid in range(n_agents)}
        positions = [(5, 5) for ag in agents]

        sim = GridSimulation(
            agents_set=agents,
            total_timesteps=n_timesteps,
            environment=CartesianEnvironment(sizes=(10, 10)),
            agents_positions=positions)
        self.assertEqual(sim.run_simulation(), n_timesteps)


        #for i in range(N_timesteps):
        #    how_many_ags_at_t = np.sum(states[i,:,:])
        #    self.assertEqual(how_many_ags_at_t, N_agents)

    def test_smelly_walker(self):
        """
        Test SmellyWalker (AvoidingWalker dropping pheromone).
        """
        n_agents = 10
        n_timesteps = 4
        agents = {SmellyWalker() for iid in range(n_agents)}
        positions = [(5, 5) for ag in agents]
        sim = GridSimulation(agents_set=agents,
                             total_timesteps=n_timesteps,
                             environment=CartesianEnvironment(sizes=(10, 10)),
                             agents_positions=positions)
        self.assertEqual(sim.run_simulation(),
                         n_timesteps)
        self.assertEqual(sim.get_environment().get_resources()['pheromone']['array']
                         .sum(),
                         n_agents*n_timesteps)
