#!/usr/bin/env python
"""Tests for `an_ant_world.environment` module."""


import unittest
import numpy as np

from an_ant_world.environment import CartesianEnvironment
from an_ant_world.agent import RandomWalker


class TestEnvironment(unittest.TestCase):
    """Tests for `an_ant_world` package."""

    def test_resources(self):
        """
        Test adding and getting resources: `.add_resources()`, `.get_resources()`.
        """
        cart = CartesianEnvironment((5, 5))
        self.assertEqual(cart.get_dimensions(), 2)
        cart.add_resources([
            {'name': 'scalar', 'dimension': 1},
            {'name': 'vector', 'dimension': 2}
            ])
        self.assertEqual(len(cart.get_resources()), 2)
        for x_cart in range(5):
            for y_cart in range(5):
                cart.get_resources()['vector']['array'][x_cart, y_cart, :] = [x_cart+y_cart, 1]
        this_ag = RandomWalker()
        cart.add_agents({this_ag}, [[2, 2]])
        vector = cart.get_view(this_ag.get_index(), 1)['vector']
        expected_vals = np.array([[2., 3., 4.], [3., 4., 5.], [4., 5., 6.]])
        self.assertTrue((vector[:, :, 0] == expected_vals).all())


    def test_actions(self):
        """
        Test executing actions with messages: `.apply_actions()`.
        """
        cart = CartesianEnvironment((5, 5))
        self.assertEqual(cart.get_dimensions(), 2)
        cart.add_resources([
            {'name': 'scalar', 'dimension': 1},
            {'name': 'vector', 'dimension': 2}
            ])
        self.assertEqual(len(cart.get_resources()), 2)
        this_ag = RandomWalker()
        cart.add_agents({this_ag}, [[2, 2]])
        # move agent
        cart.apply_actions(this_ag, [{'type': 'move', 'where': np.array([1, 1])}])
        self.assertTrue((cart.get_position(this_ag) == np.array([3, 3])).all())
        iexp = 2
        incr = 2**iexp
        for _ in range(int(incr/2)):
            cart.apply_actions(this_ag, [{'type': 'increase',
                                          'what': 'scalar',
                                          'relative_position': np.zeros(2),
                                          'how_much': 2}])
        self.assertEqual(cart.get_resources()['scalar']['array'][3, 3], 2**iexp)
        cart.apply_actions(this_ag, [{'type': 'increase',
                                      'what': 'vector',
                                      'relative_position': [1, -1],
                                      'how_much': [1.1, 0.5]}])
        self.assertTrue((cart.get_resources()['vector']['array'][4, 2] ==
                         [1.1, 0.5]).all())
