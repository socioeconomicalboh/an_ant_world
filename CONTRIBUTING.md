https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html

https://cylab.be/blog/18/gitlab-automatically-testing-your-python-project

https://www.python.org/dev/peps/pep-0008/


### How to make pull request properly

1. Fork the project

2. Go to your repo and clone it

```
git clone https://gitlab.com/<username>/an_ant_world.git
cd an_ant_world
```

3. Add upstream as new remote

```
git remote add upstream https://gitlab.com/socioeconomicalboh/an_ant_world.git
```

This way you would have two remotes

```
git remote -v
origin  https://gitlab.com:<username>/an_ant_world.git (fetch)
origin  https://gitlab.com:<username>/an_ant_world.git (push)
upstream        https://gitlab.com/socioeconomicalboh/an_ant_world.git (fetch)
upstream        https://gitlab.com/socioeconomicalboh/an_ant_world.git (push)
```

Eventually similar to that

4. Create a new branch, the name does not matter

You would most likely start from master and be up to date

```
git checkout master
git pull upstream master
```

Then create new branch

```
git checkout -b new-branch
```

If you already have one be sure to rebase it

```
git checkout new-branch
git rebase master
```

5. Make your changes, add the file and commit. Please, be specific.

```
touch file.txt
git add file.txt
git commit -m "Create file txt to show git workflow in tutorial"
```

If you made several commits, take care to squash them as much as possible (ideally there should be no "fixed typo"-type of commits, but there can be commit that describe an addition or a change in functionality). To squash multiple commits you can `reset --soft` to "undo" some of your recent commits, and then commit again as a single commit.

```
git reset --soft HEAD~3 # undo your 3 most recent commits (change 3 to your need). You changes remain, don't worry.
git status # check your changes are there and staged
git commit # make a new commit
```

Or you can squash with an interactive `rebase`:

```
# say you added 3 commits and you want to squash them onto 1
git rebase -i HEAD~3
# a file will be open with vi, with the most recent commits on top.
# change from `pick` to `squash` the commits you want to squash
# (e.g. the first 2), leaving the last one as `pick`.
# then write and exit with :wq
# then you'll be asked to write a new commit message (still in vi)
# write it and exit again with :wq
```

6. Push to your branch

DO NOT PUSH TO UPSTREAM!

```
git push origin new-branch
```

If you have already pushed on this branch, and you are pushing new updates - which you may have
squashed with one of the methods above -, then you may need to push force, with `+`:
```
git push origin +new-branch
```

7. Go to gitlab and open a Pull Request (PR) agains the branch that you need

8. If you need to make some changes to the PR just push again to the same branch


### Setup python project

Instructions on how to: obtain python3.8.x (or realize you have it), create a virtual environment with `virtualenv` (a pip-installable package), activate the virtual environment, install this project as a local library.

1. Ensure you have a python3.8 interpreter. It can be
a system installation, a pyenv installation, or an interpreter installed in some
virtual environment (e.g. a conda environment). If you have it
and you know where it lives, go to step 2.

1a. Use existing installation:

Check if you have a `python3.8` binary:

```bash
python3.8 --version
```

if you have it, check where it lives:

```bash
which python3.8
```

If you don't have it, check the versions of your `python` and/or of
your `python3` to see if they link to a python3.8:

```bash
python --version
python3 --version
```

if one of them is a `python3.8.x`, good. Check where it lives:

```bash
which python
# or
which python3
```

If you have [conda](https://docs.conda.io/projects/conda/en/latest/index.html)
on your system, you will probably have some conda environments. One of them may
already have python 3.8. If you wish to (re)use it, read on, otherwise go to 1b.
to read how to install python 3.8.

Check your conda environments with:

```bash
conda env list
# conda envs' names and locations will be shown
# you may have anonymous environments (no name)
```

you can check which python version each of them has with the one-liner:

```bash
conda env list | grep -v "^$\|#" |awk '{print $1;}'|xargs -I{} -d "\n" sh -c 'printf "Env: {}\t"; conda list -n {} |grep "^python\s";'
```

if one of them has a python version >=3.8, and you wish to reuse it as
your main interpreter for the project, check where it lives:

```bash
conda activate <env-name>
which python # remember this
conda deactivate
```

and go to step 2.

Last option: if you have [pyenv](https://github.com/pyenv/pyenv) installed, you can check which python versions you have installed through that:

```bash
pyenv versions
```

If you have a `python3.8.x` among them, you can use that. The advice for making that version the default interpreter for the project is to use:

```bash
cd /path/to/repo/root/directory
pyenv local 3.8.x # put your specific version here. e.g. 3.8.3
```

now inside the project, `python3.8` should point to something like

```bash
~/.pyenv/shims/python3.8
```

1b. Fresh python installation:

If you don't have or don't want to use an existing installation, you can
install python 3.8 anew.

You can install system-wise, via conda (a general package manager), or using pyenv (a python versions manager).

If you want a system installation, follow some guide specific for your OS. For example:

* [ubuntu with apt or from source](https://linuxize.com/post/how-to-install-python-3-8-on-ubuntu-18-04/)
* [arch/manjaro - pacman package](https://www.archlinux.org/packages/extra/x86_64/python/)
* [RHEL/centOS7/fedora31 from source](https://tecadmin.net/install-python-3-8-centos/)
* [windows](https://docs.python.org/3/using/windows.html) (good luck)
* (or just try `<your-package-manager> install python3.8` or google for how to install it)

If you want to use conda, first [install it](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html)
if you don't have it. Then, create a new conda environment with python 3.8 installed inside:

```bash
# create a new environment called py38, which uses python 3.8
# env's name and python interpreter are not related
conda create -n py38 python=3.8
# you can activate that with `conda activate py38`
```

If you want to use pyenv, first [install it](https://github.com/pyenv/pyenv#installation) if you don't have it. Usually the installation will require you to:

checkout pyenv where you want to install it (`~/.pyenv`)

```bash
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
```

and add a few lines to your `~/.bash_profile` or `~/.bashrc` or `~/.zshrc` (whatever your shell uses)

```bash
# these at the top
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
...
# these at the bottom
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi
```

and finally [install the dependencies to build python on your system](https://github.com/pyenv/pyenv/wiki#suggested-build-environment).

Once you have pyenv, you can install a specific python version with:

```bash
pyenv install 3.x.y # e.g. 3.8.3
```

which will be installed in `~/.pyenv/versions`.

2. If you have a python3.8 interpreter (and you know where it is located), you are
ready to create a virtual environment that will use that interpreter. We'll use
`virtualenv` to create the environment and `pip` to install packages.

Python comes with `pip` among its packages. So you should have that available. Check with:

```bash
pip --version
# or, if `pip` links to a python2 installation
pip3 --version 
# or
conda activate <your-conda-env-containing-python3.8>; pip --version
# or
pyenv local <3.x.y>; pip --version
```

Install `virtualenv`:

```bash
pip install virtualenv

# if you are using python and pip from within a conda environment,
# activate the conda environment first

# if you are using python and pip as installed by pyenv, first setup pyenv
# so that it knows which version you want to use, for example
# with `pyenv local <3.x.y>`
```

Create a virtual environment (called `venv`):

```bash
cd path/to/root/directory/of/this/repo

virtualenv -p python3.8 venv
# or, if `python3.8` cannot be found,
virtualenv -p /path/to/python3.8 venv # e.g. -p /usr/bin/python3.8
# or, from within an activated conda env:
(conda-env) $ virtualenv -p python3.8 venv
```

Alternative for the lazy: if you are lazy and are using PyCharm as an IDE, you can
create a virtualenv and select the python interpreter for the project by going:

```
File > Settings > Project > Project interpreter > <small gear icon> > Add > 
Virtualenv environment > New environment > Select /path/to/root/directory/of/repo/venv
as a location for the virtualenv, and browse and select a python interpreter (either
from your system or from a conda env)
```

You should now have a directory `venv` which contains some scripts in `bin` and libraries in
`lib`, including a python3.8 interpreter in `lib/python3.8`.

3. Activate the virtual environment.

From now on, whenever you have to work on the project, activate the virtual env before:

```bash
source venv/bin/activate # run the activate script
```

(if you use PyCharm, you'll find the venv automatically activated).

Test that you really have python3.8 inside, by running:

```bash
(venv) $ python --version
```

or by opening a python shell

```bash
(venv) $ python
>>> # Ctrl-D to exit
```

Once you are done you can deactivate your venv with:

```bash
deactivate
```

4. Install the project locally:

```bash
(venv) $ pip install -e .
```

This will allow you to access code from within the project's source (probably the code that lives in `src/`) via full imports. E.g.

```python
# my_file.py
from an_ant_world import SomeClass
```

instead of

```python
# my_file.py
from .. import SomeClass # relative import
```

###  Linting and testing

In order to lint with pylint:

```python
pip install pylint --quiet
pylint --ignored-classes=_socketobject src/*.py
```

Testing with unittest:

```python
python -m unittest tests/test_an_ant_world.py
```

Testing with pytest:
```python
pip install pytest --quiet
pytest
```

Run the whole test suite with podman or docker

1. install Podman or Docker on your system
2. Run `./run-test.sh test` to build and run the test
3. For more options `./run-test.sh -h`

There is an experimental feature to run docker cloning the project from upstream/master. In order to do that copy your priv ssh key to `./ssh-key/id_rsa` and run `build` and `run` commands using `-f Dockerfile.git`

The ssh key should be excluded but be aware to name it correctly and not add it to the staged files!


