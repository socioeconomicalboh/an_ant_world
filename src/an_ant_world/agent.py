"""
The module of agents.
"""
from abc import ABC, abstractmethod
import numpy as np

class Agent(ABC):
    """
    abstract class representing a generic agent
    """

    _index = 0

    def __init__(self):
        Agent._index += 1
        self._index = Agent._index

    @abstractmethod
    def act(self, view):
        """TODO docstring
        """

    @abstractmethod
    def get_local_info(self):
        """TODO docstring
        """

    @abstractmethod
    def get_requirements(self):
        """
        Return the requirements of the agent for the Environment.
        """
        return {}

    def get_index(self):
        """
        Simple getter for _index
        """
        return self._index


class RandomWalker(Agent):
    """
    Agent that only walks randomly
    """
    def __init__(self, sight=2):
        super().__init__()
        self._sight = sight
        self._requirements = []

    def act(self, view):
        messages = []
        messages.append(step_random(view, avoid=False))
        return messages


    def get_local_info(self):
        return {
            'index': self._index,
            'sight': self._sight
                }

    def get_requirements(self):
        return self._requirements




class AvoidingWalker(RandomWalker):
    """
    Agent that avoid other agents and walls
    """
    all_directions = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9])

    def __init__(self, sight=2):
        super().__init__(sight=sight)

    def act(self, view):
        messages = []
        messages.append(step_random(view, avoid=True))
        return messages


class SmellyWalker(RandomWalker):
    """
    Random walker releasing pheromone where it passes.
    """
    def __init__(self, sight=2):
        super().__init__(sight)
        self._requirements = [{
            'name': 'pheromone',
            'dimension': 1
            }]

    def act(self, view):
        messages = []
        messages.append(step_random(view, avoid=True))
        messages.append(drop_pheromone(view))
        return messages

    def get_requirements(self):
        return self._requirements



def step_random(view, avoid=True):
    """
    Returns a message which requests to move the agent:
    - 1/9 probability of staying still;
    - 1/8 probability of taking a given direction.
    If `avoid` is true, the places already occupied are not taken into account.
    """
    # - make it more specific (e.g. avoid agents walking, but not those flying)
    shape_view = np.array(view['space'].shape)
    shape_directions = np.array([3  for sz in shape_view])
    direction_matrix = np.zeros(np.prod(shape_directions)).reshape(shape_directions)
    possible_directions = list()
    for coord, _ in np.ndenumerate(direction_matrix):
        direction = np.array(coord)-1
        space_value = view['space'][
            tuple([slice(s, s+1)
                   for s in (shape_view/2).astype(int)+direction])
            ].reshape(1)[0]
        if space_value == 0:
            possible_directions.append(direction)
        if (not avoid and space_value >= 0):
            possible_directions.append(direction)
    if len(possible_directions) < 1:
        step = np.array([0 for sz in shape_view])
    else:
        step = (possible_directions[
            int(np.random.randint(0, len(possible_directions)))
            ])
    return {'type': 'move', 'where': step}

def drop_pheromone(view):
    """
    Returns a messages which requests that pheromone is increased where the walker steps.
    """
    return {'type': 'increase',
            'what': 'pheromone',
            'relative_position': np.zeros(len(view['space'].shape)),
            'how_much': 1}
    #view['pheromone'][self._sight, self._sight] += 1
