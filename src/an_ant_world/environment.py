"""
TODO docstring`
"""
from abc import ABC, abstractmethod
import numpy as np

class Environment(ABC):
    """abstract class representing the environment in which `Agent`s are moving
    """

    def get_resources(self):
        """
        Simple getter.
        """

    @abstractmethod
    def add_resources(self, resources):
        """TODO docstring
        """

    @abstractmethod
    def apply_actions(self, agent, messages):
        """
        The actions made by agents are in the dictionary `messages`.
        """

    @abstractmethod
    def get_view(self, info_id, info_sight): #local_info):
        """
        Get a view of the resources (and space) relative to local_info.
        """

    @abstractmethod
    def get_agents(self):
        """
        Get snapshot of the agents.
        """


class CartesianEnvironment(Environment):
    """
    Environment using Cartesian coordinates
    """

    def __init__(self, sizes):
        """
        `sizes` is a tuple with the size for each Cartesian dimension
        """
        super().__init__()
        self._sizes = sizes
        self._dimensions = len(sizes)
        self._space = np.zeros(np.multiply(*self._sizes)).reshape(*self._sizes)
        self._agents_positions = {}
        self._resources = {}

    def get_dimensions(self):
        """
        Simple getter.
        """
        return self._dimensions

    def get_sizes(self):
        """
        Simple getter.
        """
        return self._sizes

    def add_agents(self, agents_set, positions):
        """
        Process the agents in order to dinamicaly adapt the environment.
        """
        for agent, position in zip(agents_set, positions):
            self._agents_positions[agent.get_index()] = np.array([int(p) for p in position])
            self.add_resources(agent.get_requirements())
            self._space[position] += 1

    def get_position(self, agent):
        """
        Return position of the given agent.
        """
        return self._agents_positions[agent.get_index()]

    def apply_actions(self, agent, messages):
        """
        The actions made by agents are in the dictionary `messages`.
        """
        for msg in messages:
            msg_type = msg['type']
            if msg_type == 'move': # v = relative position
                ###print(self._space)
                agent_position = self._agents_positions[agent.get_index()]
                #print('before moving', agent_position)
                #print(type(agent_position))
                self._space[tuple(agent_position)] -= 1
                agent_position += msg['where']
                #print('after moving', agent_position)
                self._space[tuple(agent_position)] += 1
                #self._agents_positions[agent.get_index()] += msg['where']

            elif msg_type == 'increase':
                resource_to_incr = msg['what']
                relative_pos = msg['relative_position']
                incr_value = msg['how_much']
                ag_pos = self._agents_positions[agent.get_index()]
                # select the position corresponding to the position
                #  of the agent, translated according to the message:
                #  relative_pos is a space-dimensional vector.
                indices = tuple([
                    slice(ag_pos[dim]+int(relative_pos[dim]),
                          ag_pos[dim]+int(relative_pos[dim])+1)
                    for dim in range(len(self._sizes))
                    ])
                self._resources[resource_to_incr]['array'][indices] += incr_value


    def _create_indices_for_slicing(self, pos, rng):
        """
        Create the indices for slicing:
           for every dimension in space, the slice ranges
           below and above the position of the agent,
           with a "radius" equal to the agent's sight.
           If this slices exceed the boundaries,
           the sites in excess are assigned a value of -1.
        """
        span = 2*rng + 1
        limits = []
        for idx, sze in enumerate(self._sizes):
            # where to copy from resources
            resource_slice_1d = slice(max(0, pos[idx]-rng),
                                      min(self._sizes[idx], pos[idx]+rng+1))
            # where to paste into view matrix
            view_slice_1d = slice(
                0 if pos[idx] - rng > 0 else -(pos[idx] - rng),
                span if sze > (pos[idx] + rng) else span-((pos[idx] + rng - sze))-1
            )
            limits.append({'resource_slice_1d': resource_slice_1d,
                           'view_slice_1d': view_slice_1d})
        return (tuple([limits[idx]['resource_slice_1d']
                 for idx, _ in enumerate(self._sizes)]),
                tuple([limits[idx]['view_slice_1d']
                 for idx, _ in enumerate(self._sizes)]))


    def get_view(self, info_id, info_sight): #local_info):
        """
        Returns the view of the environment (dict with 
            {'resource': matrix  of values} for a given agent
            (e.g. the food in its surroundings, the space).
        """
        ag_idx = info_id #local_info['index']
        span = 2*info_sight + 1 #2*local_info['sight'] + 1
        resource_slice, view_slice = self._create_indices_for_slicing(
            self._agents_positions[ag_idx], info_sight)
        # add resources + space to return_dict
        #  each view is created as a matrix of "-1"s
        #  the actually visible part is copied into view
        #  the remaining "-1"s correspond to out of boundary
        return_dict = {}
        for rname, rdict in self._resources.items():
            rarr = rdict['array']
            rdim = rdict['dimension']
            # 
            res_view = rarr[resource_slice]
            # 
            list_shapes = [span for sze in self._sizes]
            list_shapes.append(rdim)
            # 
            view = np.zeros(shape=list_shapes) - 1
            view[view_slice] = res_view
            return_dict.update({rname: view})

        space_view = self._space[resource_slice]
        view = np.zeros(shape=[span for sze in self._sizes]) - 1
        view[view_slice] = space_view
        return_dict.update({'space': view})
        return return_dict

    def add_resources(self, resources):
        """
        adds to the environment the containers corresponding to the
        resources (e.g. food, pheromone)
        """
        for res in resources:
            rname = res['name']
            if rname not in self._resources:
                sizes = list(self._sizes)
                sizes.append(res['dimension'])
                # setattr not good because need to iterate over _resources
                self._resources[rname] = {
                    'array': np.zeros(shape=sizes),
                    'dimension': res['dimension']
                }

    def get_resources(self):
        """
        Get a snapshot of the resources array.
        """
        return self._resources

    def get_agents(self):
        """
        Get a snaphot of the agents.
        """
        return {
            'agents_positions': self._agents_positions
        }
