"""
TODO docstring
"""
from abc import ABC, abstractmethod
import copy

class Simulation(ABC):
    """
    abstract class for instancing a simulation
    """

    def __init__(self):
        self._tale = list()

    @abstractmethod
    def run_simulation(self):
        """TODO Docstring
        """

    @abstractmethod
    def get_agents(self):
        """TODO Docstring
        """

    @abstractmethod
    def get_environment(self):
        """TODO Docstring
        """

    @abstractmethod
    def _write_tale(self):
        """
        Compose self._tale.
        """

class GridSimulation(Simulation):
    """
    Class for instancing a simulation on a 2-dimensional grid
    """
    def __init__(self, agents_set, total_timesteps, environment, agents_positions):
        super().__init__()
        self._current_time = 0
        self.initialize_simulation(agents_set, total_timesteps, environment, agents_positions)

    def initialize_simulation(self, agents_set, total_timesteps, environment, agents_positions):
        """
        Initialize the simulation with agents and an environment.
        """
        self._current_time = 0
        self._total_timesteps = total_timesteps
        self._environment = environment
        self._agents_set = agents_set
        environment.add_agents(agents_set, agents_positions)
        self._write_tale()


    def run_simulation(self):
        for t_current in range(self._total_timesteps):
            self._current_time = t_current
            for agent in self._agents_set:
                # get position of agent and feed it into environment to get agent's view
                info = agent.get_local_info()
                self._environment.apply_actions(agent, agent.act(
                    self._environment.get_view(info['index'], info['sight'])))
            self._write_tale() # save data
        self.end_simulation()
        return self._total_timesteps

    def end_simulation(self):
        """TODO docstring
        """
        #print("END SIMULATION: ", self._total_timesteps, " timesteps.")

    def get_agents(self):
        return self._agents_set

    def get_environment(self):
        return self._environment

    def _print_current_environment(self, outfile=None):
        pass

    def _write_tale(self):
        ag_pos = copy.deepcopy(self._environment.get_agents()['agents_positions'])
        snapshot = {'agents_positions': ag_pos}
        self._tale.append(snapshot)
