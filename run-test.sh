#!/bin/bash

DOCKER_COMMAND="sudo docker"

TAG_BUILD=""
COMMAND=""
DOCKER_FILE=""
IT=""

# Parse options to the command
while getopts ":ph" opt; do
    case ${opt} in
        h )
            echo "Usage:"
            echo "    ./run-test.sh -h      Display this help message"
            echo "    ./run-test.sh test    Build and run default"
            echo "    ./run-test.sh build   Build container"
            echo "                       -f Specify dockerfile"
            echo "                          default ."
            echo "                       -p Podman"
            echo "                       -t Specify tag for build"
            echo "                          default an_ant_world:latest"
            echo "    ./run-test.sh run     Run container"
            echo "                       -p Podman"
            echo "                       -s Interactive mode"
            echo "                       -t Specify tag for run"
            echo "                          default an_ant_world:latest"
            exit 0
            ;;
        \? )
            echo "Invalid Option: -$OPTARG" 1>&2
            exit 1
            ;;
    esac
done
shift $((OPTIND -1))  # Remove ./run-test.sh from the argument list

subcommand=$1; shift
case "$subcommand" in
    build)
    COMMAND="build"

    TAG_BUILD="-t an_ant_world:latest"
    DOCKER_FILE="."
    IT=""

    echo "$@"
    while getopts ":f:pt:" opt; do
        case ${opt} in
            f )
                DOCKER_FILE="-f $OPTARG ."
                ;;
            p )
                DOCKER_COMMAND="podman"
                ;;
            t )
                TAG_BUILD="-t an_ant_world:"$OPTARG
                ;;
            \? )
                echo "Invalid Option: -$OPTARG" 1>&2
                exit 1
                ;;
            : )
                echo "Invalid Option: -$OPTARG requires an argument" 1>&2
                exit 1
                ;;
        esac
    done
    shift $((OPTIND -1))
    ;;

    run)
        COMMAND="run"

        DOCKER_FILE=""
        TAG_BUILD="an_ant_world:latest"
        IT=""

    while getopts ":pst:" opt; do
        case ${opt} in
            p )
                DOCKER_COMMAND="podman"
                ;;
            s ) IT="-it"
                ;;
            t )
                TAG_BUILD="an_ant_world:$OPTARG"
                ;;
            \? )
                echo "Invalid Option: -$OPTARG" 1>&2
                exit 1
                ;;
            : )
                echo "Invalid Option: -$OPTARG requires an argument" 1>&2
                exit 1
                ;;
        esac
    done
    shift $((OPTIND -1))
    ;;

    test )
        sudo docker build -t an_ant_world:latest . && sudo docker run an_ant_world:latest
        exit 0
        ;;
esac

TO_EXEC="${DOCKER_COMMAND} ${COMMAND} ${IT} ${TAG_BUILD} ${DOCKER_FILE}"
echo "Executing $TO_EXEC"
${TO_EXEC}
